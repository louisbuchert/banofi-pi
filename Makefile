.PHONY: shell_bitbake docker_build docker_run

shell_bitbake: docker_build docker_run

docker_build:
	docker build --build-arg UID=$(shell id -u) --build-arg GID=$(shell id -g) -t banofipi-yocto -f docker/Dockerfile docker

docker_run: 
	docker run -it \
        -v ${CURDIR}:${PWD}:shared \
        -v ${HOME}/.ssh:/home/ubuntu/.ssh:ro \
        -v ${HOME}/.gitconfig:/home/ubuntu/.gitconfig:ro \
        -w ${PWD} \
        banofipi-yocto \
        /bin/bash

